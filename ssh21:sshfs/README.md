# SSH
## ASIX M06-ASO 2021-2022

Imatges docker al DockerHub de [edtasixm06](https://hub.docker.com/u/edtasixm06/)

Documentació del mòdul a [ASIX-M06](https://sites.google.com/site/asixm06edt/)

ASIX M06-ASO Escola del treball de barcelona

### SSH Containers:

 * **ssh21:sshfs** Host client que accedeix al servidor SSH. Aquest host client està configurat per muntar els homes dels usuaris via sshfs del servidor SSH. S'ha configurat syste-auth per usar *pam_mount* i configurat *pam_mount.conf.xml* per muntar un recurs de xarxa al home dels usuaris via SSHFS.

    Atenció, cal usar en el container --privileged per poder fer els muntatges nfs.


